<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\NoteController;
use App\Http\Controllers\Front\ReportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', [NoteController::class, 'index'])->name('index');
Route::get('/note/{note}', [NoteController::class, 'showNote'])->name('showNote');
Route::GET('/type',[ReportController::class, 'type'])
->name('type');
Route::get('/reports',function () {
    return view('reports');
})->name('reports');
Route::get('/table', [ReportController::class, 'table'])->name('table');

Route::group(['middleware'=>'auth'],function (){
    Route::get('/dashboard', [NoteController::class, 'dashboard'])->name('dashboard');
    Route::delete('/destroy/{id}', [NoteController::class, 'destroy'])->name('destroy');
    Route::post('/store', [NoteController::class, 'store'])->name('store');
    Route::get('/create',function () {
        return view('create');
    })->name('create');

    Route::get('/edit/{note}',[NoteController::class,'edit'])->name('edit');
    Route::put('/update/{id}',[NoteController::class,'update'])->name('update');

});
require __DIR__.'/auth.php';
