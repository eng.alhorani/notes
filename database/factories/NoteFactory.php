<?php

namespace Database\Factories;
use App\Helpers\FakerHelper;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class NoteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $collection = collect(['on date', 'normal', 'urgent']);

        $shuffled = $collection->shuffle();

        $type = $shuffled->first();






    $f = new FakerHelper();
    $height = rand(600, 900);
    $width = intval($height * 2 / 3);

        return [
            'title'=>$this->faker->name,
        'content'=>$this->faker->sentence(100),
        'user_id'=>User::all()->random()->id,
        'type' => $type,
        'cover_image' => $f->imageUrl($width, $height, 'NOT USED', false),
        ];
    }
}
