<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Cms</title>
    @include('includes._header')
</head>
<body class="antialiased">
@include('includes._nav')

<div class="container">
    <div class="w-75 mx-auto">
        <div class="w-75 mx-auto pt-4">
           <img src="{{ storageImage($note->cover_image) ? storageImage($note->cover_image) : asset('assets/img/no-image.png') }}" class="mx-auto w-75">
        </div>
        <div class="w-75  pt-4 text-center">
            <h1>{{$note->title}}</h1>
        </div>
        <div class="w-75 mx-auto pt-4">
            <p>
                {!! strip_tags($note->content) !!}
            </p>
        </div>
        <div class="w-75  pt-4 text-center">
            <h1>{{$note->type}}</h1>
        </div>

    </div>


</div>

@include('includes._scripts')
</body>
</html>
