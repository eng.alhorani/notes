<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Cms</title>
  @include('includes._header')
</head>
    <body class="antialiased">
    @include('includes._nav')
    <div class="container bg-gray-100" >
        <div class="w-100 my-4">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <h2 class="color-black">Notes</h2>

        </div>
        <div class="d-flex flex-wrap justify-content-around">
            @foreach($notes as $note)
                <div class="card my-2 mx-2 " style="width: 18rem">
                    {{-- <img src="{{storageImage($note->cover_image)}}" class="card-img-top" alt="..." > --}}
           <img src="{{ storageImage($note->cover_image) ? storageImage($note->cover_image) : asset('assets/img/no-image.png') }}" class="card-img-top" style="height: 200px">

                    <div class="card-body">
                        <h5 class="card-title">{{$note->title}}</h5>
                        <p class="card-text">   {!!   Illuminate\Support\Str::limit( strip_tags($note->content), 50) !!}...
                        </p>
                        <a href="{{route('showNote',$note)}}" class="btn btn-primary">show</a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="w-100 py-2">
            {{ $notes->links() }}
        </div>

    </div>

       @include('includes._scripts')
    </body>
</html>
