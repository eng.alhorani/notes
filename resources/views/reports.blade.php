<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <a href="{{route('index')}}"> <title>NOTES</title> </a>
    @include('includes._header')
</head>
<body class="antialiased">
@include('includes._nav')

<div class="container">
 
    <div class="row">
        <div class="col-6">
            <div id="chart_2"></div>
        </div>
        <div class="col-6">
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>numberNotes</th>
                        <th>type</th>
                        
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>numberNotes</th>
                        <th>type</th>
                       
                    </tr>
                </tfoot>
            </table>



        </div>
       
    </div>

</div>
@include('includes._scripts')
</body>
</html>
