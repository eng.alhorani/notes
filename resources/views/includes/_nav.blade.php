<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">NOTES</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

        </ul>
        <div class="form-inline my-2 my-lg-0">
            @if (Route::has('login'))
                <div  class="d-flex">
                    @auth
                        <a href="{{ url('/dashboard') }}" class="mx-1 btn btn-link">Dashboard</a>
                        <a href="{{ url('/reports') }}" class="mx-1 btn btn-link">reports</a>

                        <a href="{{ url('/') }}" class="mx-1 btn btn-link">home</a>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf


                            <button type="submit" class="btn btn-link px-1 mt-0" >{{ __('Log Out') }} </button>
                        </form>
                    @else
                        <a href="{{ route('login') }}" class="mx-1">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="mx-1">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        </div>
    </div>
</nav>
