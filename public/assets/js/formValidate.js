
console.log("hi");

jQuery(document).ready(function() {
    let $registerForm =$('#formId');
    if ($registerForm.length) {
        $registerForm.validate({
            rules: {
                title: {
                    required: true
                },
                content: {
                    required: true,
                },
                type: {
                    required: true,
                },

            },
            messages: {
                'title': {
                    required: 'please enter title'
                },
                'content': {
                    required: 'please enter content'
                },
                'type': {
                    required: 'please select value'
                },

            }
        });
    }
});
