<?php

namespace App\Repositories;

use App\Models\Note;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;
use function auth;
//use function convertToSeparatedTokens;

class NoteRepository {

        public function add(Request $request)
        {
            $note = Note::create([
                'title' => $request->title,
                'content' => $request->content ,
                'type' => $request->type,
                'user_id' => $request->user_id,
            ]);

            $note->cover_image = uploadFile('cover_image', 'note');

            $note->save();

        }

    public function getNotes(Request $request)
    {
        $notes = Note::query();

        if ($search = $request->get('search')) {
            $tokens = convertToSeparatedTokens($search);
            $notes->whereRaw("MATCH(title, content) AGAINST(? IN BOOLEAN MODE)", $tokens);
        }
        return $notes;
    }


    public function update(Request $request, Note $note)
    {
        $note->update($request->all());
        if ($request->hasFile('cover_image')) {
            // if there is an old background_image delete it
            if ($note->cover_image != null) {
                $note->cover_image = Storage::disk('public')->delete($note->cover_image);
            }
            $note->cover_image = uploadFile('cover_image', 'note');
        }
    $note->save();
        return $note;
    }


    public function delete(Note $note)
    {
//        if ($note->cover_image != null)
//            $note->cover_image = Storage::disk('public')->delete($note->cover_image);

        $note->delete();
    }

}
