<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoteRequest;
use App\Models\Note;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Repositories\NoteRepository;
class NoteController extends Controller
{
    private  $noteRepository;
    public function __construct(NoteRepository $noteRepository)
    {
        $this->noteRepository = $noteRepository;
    }
    public function index()
    {
        $notes = Note::orderBy('created_at','desc')->paginate(9);

        return view('home', compact('notes'));
    }
    public function showNote(Note $note)
    {
        return view('showNote', compact('note'));
    }
//dashboard
    public function dashboard(Request $request)
    {
        $user = User::query()->findOrFail(auth()->id());
        $notes = Note::query()->whereUserId($user->id)->paginate(10);
        return view('dashboard',compact('notes'));
    }
//delete
    public function destroy($id, Request $request)
    {
        $note = Note::query()->find($id);

        $this->noteRepository->delete($note);

      $request->session()->flash('success','note deleted successfully');

        return redirect(route('dashboard'));
    }
// store
    public function store(NoteRequest $request)
    {
        $this->noteRepository->add($request);

        return redirect(route('dashboard'));
    }
//edit
    public function edit(Note $note)
    {
     return   view('create',compact('note'));
    }
    public function update(NoteRequest $request,$id)
    {
        $note = Note::query()->find($id);
        $this->noteRepository->update($request, $note);

        return   redirect(route('dashboard'));
    }

}
