<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Models\Note;
use DataTables;
class ReportController extends Controller
{
    
    public function type():JsonResponse
    {
        $notes = DB::table('notes')
            ->select(DB::raw('count(title) as numberNotes,  type'))
            ->groupBy('type')
            ->orderBy('type')
            ->get();
        $type =[];$i=0;
        $noteNumber =[];
        foreach ($notes as $data)
        {
            array_push($type,$notes[$i]->type);
            array_push($noteNumber,$notes[$i]->numberNotes);
            if (sizeof($notes) - 1 > $i)
                $i++;
        }
        

        return response()->json(['type'=>$type,'noteNumber'=>$noteNumber]);
    }
    public function table():JsonResponse
    {
        $data = DB::table('notes')
            ->select(DB::raw('count(title) as numberNotes,  type'))
            ->groupBy('type')
            ->orderBy('type')
            ->get();
            // return response()->json($datas);
            return response()->json(['data' => $data]);
    }

}
