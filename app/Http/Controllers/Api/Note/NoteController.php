<?php

namespace App\Http\Controllers\Api\Note;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Note;
use Illuminate\Http\Request;
use App\Http\Requests\NoteRequest;

use App\Repositories\NoteRepository;
use Illuminate\Http\JsonResponse;

class NoteController extends  ApiController
{
    private $noteRepository;

    public function __construct(NoteRepository $noteRepository){
        $this->noteRepository = $noteRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): JsonResponse
    {
        $limit = $request->get('limit') ? : 10 ;
        if($limit > 30 ) $limit =30 ;
        $request->request->add(['status' => 1]);

        $notes = $this->noteRepository
            ->getNotes($request)
            ->paginate($limit);
        $notesPage= $notes->all();
        return $this->respondSuccess($notesPage, $this->createApiPaginator($notes));

    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(NoteRequest $request): JsonResponse
    {

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));
        $request->request->add(['user_id' => $user->id]);
        $this->noteRepository->add($request);

        return $this->respondSuccess();
    }



    public function note($id, Request $request): JsonResponse
    {

        $note = Note::query()
            ->where('id', $id)
            ->first();

        if (!$note)
            return $this->respondError(__('api.note_not_found'));


        return $this->respondSuccess($note);
    }





    public function myNotes(Request $request): JsonResponse
    {
        $limit = $request->get('limit') ? : 10 ;
        if($limit > 30 ) $limit =30 ;

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondSuccess(__('api.user_not_found'));

        $notes = Note::query()->whereUserId($user->id)->paginate($limit);


        return $this->respondSuccess($notes->all(), $this->createApiPaginator($notes));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, NoteRequest $request): JsonResponse
    {
        $note = Note::query()->find($id);
        if (!$note)
            return $this->respondError(__('api.item_not_found'));

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));

        if ($user->id != $note->user->id)
            return $this->respondError(__('api.unauthorized'));

        $this->noteRepository->update($request, $note);

        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request): JsonResponse
    {
        $note = Note::query()->find($id);
        if (!$note)
            return $this->respondError(__('api.item_not_found'));

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));

        if ($user->id != $note->user->id)
            return $this->respondError(__('api.unauthorized'));

        $this->noteRepository->delete($note);

        return $this->respondSuccess();
    }


}
